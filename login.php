<?php
require_once "pdoadapter.php";
require_once "hashGenerate.php";
require_once "domainsettings.php";

// $input = file_get_contents('php://input');
// $data = json_decode($input, true);            //        для arc
//$data = $_POST; //        для js
$data = json_decode($_POST['postedData'], true);

if (isset($data['password']) || (isset($data['login'])))
{
    if (isset($data['login'])) 
    {
        $input = $data['login'];
        $type = 'login';
    }
    $queryAttrs = array($input, 1);
    $stmt = DB::run("SELECT * FROM users WHERE ".$type." = ? LIMIT ?", $queryAttrs)->fetch();

    if (!$stmt)
    {
        echo '-1';
        die;
    }

    $salt = $stmt['salt'];
    $hash = $stmt['hash'];
    $newhash=hashGenerate($data['password'], $salt);
    if ($newhash != $hash) {
        echo '-1';
        die;
    }

    session_start();
    $_SESSION = array(
        'login' => $stmt['login'],
        'password' => $data['password'],
        'isAdmin' => $stmt['isAdmin']
    );
    setcookie("isAdmin",  $stmt['isAdmin']?'true':'false', time()+60*60*24*30);
    setcookie("login", $stmt['login'], time()+60*60*24*30);
    setcookie("password", $data['password'],time()+60*60*24*30);
    setcookie("firstname",$stmt['firstname'],time()+60*60*24*30);

    echo '0';
    die;
}

echo '-1';
?>