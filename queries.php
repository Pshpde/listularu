<?php
require_once "pdoadapter.php";
require_once "hashGenerate.php";

// $input = file_get_contents('php://input');
// $data = json_decode($input, true);            //        для arc
$data = json_decode($_POST['postedData'], true); //        для js

if (!isset($_SESSION)) {
    session_start();
}
if (isset($data['args'])) {
    echo call_user_func_array($data['method'], $data['args']);
} else {
    echo call_user_func($data['method']);
}


function checkCookies($login, $isAdmin, $password) {
    function getOut(){
        include "logout.php";
        echo 'error';
        die();
    }

    $query="SELECT * FROM users WHERE ";
    $query=$query."login ='".$login."' AND isAdmin=".$isAdmin." LIMIT 1";
    
    $stmt = DB::run($query)->fetch();
    if (!$stmt) {
        getOut();
        return '-1';
    }
    if (hashGenerate($password, $stmt['salt']) != $stmt['hash']) {
        getOut();
        return '-1';
    }
    setcookie("isAdmin",  $stmt['isAdmin']?'true':'false', time()+60*60*24*30);
    setcookie("login", $stmt['login'], time()+60*60*24*30);
    setcookie("password", $password,time()+60*60*24*30);
    setcookie("firstname",$stmt['firstname'],time()+60*60*24*30);
    return $stmt;
}

function getUserData($login, $isAdmin, $password){
    $checkRes=checkCookies($login,$isAdmin,$password);
    if($checkRes==='-1')
        die;
    $query="SELECT * FROM users WHERE ";
    $query=$query."login ='".$login."' AND isAdmin=".$isAdmin." LIMIT 1";
        
     $stmt = DB::run($query)->fetch();
    $arr=array(
        'firstname'=>$stmt['firstname'],
        'lastname'=>$stmt['lastname'],
        'secondname'=>$stmt['secondname'],
        'login'=>$stmt['login'],
        'email'=>$stmt['email'],
        'position'=>$stmt['position'],
        'ngroup'=>$stmt['ngroup'],
        'isAdmin'=>$stmt['isAdmin']
    );
    $message=json_encode($stmt);
    echo $message;
}


?>