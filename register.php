<?php
require_once "pdoadapter.php";
require_once "hashGenerate.php";

// $input = file_get_contents('php://input');
// $data = json_decode($input, true);            //        для arc
//$data = $_POST; //        для js
$data = json_decode($_POST['postedData'], true);


if (!isset( $data['lastname'], 
            $data['firstname'], 
            $data['email'], 
            $data['login'],
            $data['password']))
{
    echo '-1';
    die;
}
if(!preg_match("/^[a-zA-Z0-9]+$/",$data['login']))
{
    echo '-1';
    die;
}
$queryAttrs = array($data['login'], $data['email'], 1);
$stmt = DB::run("SELECT login, email FROM users WHERE login ='".$data['login']."' OR email = '".$data['email']."' LIMIT 1")->fetch();

if ($stmt)
{
    echo '-1';
    die;
}

$salt = uniqid(mt_rand(), true);

$position=strtolower($data['position']);
if($position=='студент' || $position=='student')
{
    $position='Студент';
    $groupnumber=$data['group'];
}
else
{
    $groupnumber='';
}

$queryAttrs = array(
    'firstname' => $data['firstname'],
    'group'     => $data['group'],
    'lastname' => $data['lastname'],
    'secondname' => $data['secondname'],
    'login' => $data['login'],
    'email' => $data['email'],
    'hash' => hashGenerate($data['password'], $salt),
    'salt' => $salt,
    'position' => $position,
    'isAdmin' => 'false'

);

$stmt = DB::run("INSERT INTO users (firstname, ngroup, lastname, secondname, login, email, hash, salt, position, isAdmin) VALUES ('".$queryAttrs['firstname']."', '".$queryAttrs['group']."', '".$queryAttrs['lastname']."',  '".$queryAttrs['secondname']."', '".$queryAttrs['login']."', '".$queryAttrs['email']."', '".$queryAttrs['hash']."', '".$queryAttrs['salt']."', '".$queryAttrs['position']."', ".$queryAttrs['isAdmin'].")");
echo '0';
?>