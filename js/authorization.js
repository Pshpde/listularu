function register() {
    var firstname = $("#firstname").val();
    var lastname = $("#lastname").val();
    var secondname = $("#secondname").val();
    var login = $("#reglogin").val();
    var email = $("#email").val();
    var password = sha256($("#regpassword").val());
    var position = $("#position").val();
    var group = $("#group").val();

    var arr = {
        lastname : lastname,
        firstname: firstname,
        secondname : secondname,
        email : email,
        login : login,
        password: password,
        position: position,
        group:group
    }
    
    var postedData = JSON.stringify(arr);
    
    $.ajax({
        type: "POST",
        url: "./register.php",
        data: {postedData},
        success: function(html){ 
            if (html != "0") {
                alert("Ошибка при регистрации.");
            } else {
                alert("Аккаунт успешно создан, Вы можете войти.");
            }
        }
    });
}

function Login() {
    var login = $("#login").val();
    var password = sha256($("#password").val());

    var arr = {
        login : login,
        password: password
    }
    var postedData = JSON.stringify(arr);
    $.ajax({
        type: "POST",
        url: "./login.php",
        data: {postedData},
        success: function(html){ 
            if(html!="0")
            {
                alert("Введен неправильный логин или пароль");
            }
            else 
            {
                location.reload();
            }
        }
    });
}

function Logout() {
    $.ajax({
        type: "POST",
        url: "logout.php",
        data: {},
        success: function(html){ 
            location.reload();
        }
    });
}

function passCode() {
    var password = sha256($("#password").val().trim());
    $("#password").val(password);
}

function checkCookies(){
    var login = getCookie("login");
    var firstname = getCookie("firstname");
    var password = getCookie("password");
    var isAdmin = getCookie("isAdmin");
    
    if(login==undefined || firstname==undefined ||password==undefined||isAdmin==undefined)
        {
            Logout();
            return '-1';
        }

    var arr = {
        method: 'checkCookies',
        args: {
            login: login,
            isAdmin: isAdmin,
            password: password
        }
    }

    var postedData = JSON.stringify(arr);
    var request = $.ajax({
        type: "POST",
        url: "queries.php",
        data: {postedData},
        success: function(html){ 
            if (html == 'error') {
                window.location.replace("./index.html");
                return html;
            }
        },
        error: function() {
            alert("Ошибка сети.");
            return html;
        }
    });
}

$(function(){ 
    var login = getCookie("login");
    var firstname = getCookie("firstname");
    if ((login !== undefined)&&(firstname!==undefined)) {
        $("#user-info").show();
        $("#user-info-content").html(`Добро пожаловать, ${firstname}`);
        $('#logoutButton').show();
    }
    else $('#loginButton').show();
});

