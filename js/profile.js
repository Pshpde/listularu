function onProfileLoad(){

    var login = getCookie("login");
    var password = getCookie("password");
    var isAdmin = getCookie("isAdmin");
    var arr = {
        method: 'getUserData',
        args: {
            login: login,
            isAdmin: isAdmin,
            password: password
        }
    }
    var postedData = JSON.stringify(arr);
    var request = $.ajax({
        type: "POST",
        url: "queries.php",
        data: {postedData},
        success: function(html){ 
            if (html == 'error') {
                window.location.replace("./index.html");
                return html;
            }
            else{
                var resolvedData = JSON.parse(html);
                if(getCookie("isAdmin")=='true')
                    $('#admin-menus').show();
                else $('#admin-menus').hide();
                $('#lastname-profile')
                    .text(resolvedData['lastname']);
                $('#firstname-profile')
                    .text(resolvedData['firstname']);
                $('#secondname-profile')
                    .text(resolvedData['secondname']);
                $('#E-Mail-profile')
                    .text(resolvedData['email']);
                $('#position-profile')
                    .text(resolvedData['position']); 
                $('#ngroup-profile')
                    .text(resolvedData['ngroup']); 
            }
        },
        error: function() {
            alert("Ошибка сети.");
            return html;
        }
    });
    
    
}

function getProfileInfo(){
    
}