create table users
(
	firstname text,
    lastname text,
    secondname text,
    login text NOT NULL,
    email text,
    hash text NOT NULL,
    salt text NOT NULL,
    position text,
    isAdmin boolean,
    ngroup text,
    constraint Users_pk primary key (login)
)