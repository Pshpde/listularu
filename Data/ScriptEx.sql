/*==============================================================*/
/* DBMS name:      Sybase AS Anywhere 9                         */
/* Created on:     30.04.2008 17:28:56                          */
/*==============================================================*/


if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Participate_FK'
     and t.table_name='Author'
) then
   drop index Author.Participate_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Relationship_17_FK'
     and t.table_name='Author'
) then
   drop index Author.Relationship_17_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Relationship_12_FK'
     and t.table_name='Book'
) then
   drop index Book.Relationship_12_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Relationship_16_FK'
     and t.table_name='Book'
) then
   drop index Book.Relationship_16_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Relationship_19_FK'
     and t.table_name='Book'
) then
   drop index Book.Relationship_19_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Create_book2_FK'
     and t.table_name='Create_book'
) then
   drop index Create_book.Create_book2_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Create_book_FK'
     and t.table_name='Create_book'
) then
   drop index Create_book.Create_book_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Create_paper2_FK'
     and t.table_name='Create_paper'
) then
   drop index Create_paper.Create_paper2_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Create_paper_FK'
     and t.table_name='Create_paper'
) then
   drop index Create_paper.Create_paper_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Book_belong_FK'
     and t.table_name='Genre/Style'
) then
   drop index "Genre/Style".Book_belong_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Relationship_11_FK'
     and t.table_name='Genre/Style'
) then
   drop index "Genre/Style".Relationship_11_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Speak_FK'
     and t.table_name='Language'
) then
   drop index Language.Speak_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Translate_book_FK'
     and t.table_name='Language'
) then
   drop index Language.Translate_book_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Translate_paper_FK'
     and t.table_name='Language'
) then
   drop index Language.Translate_paper_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Live2_FK'
     and t.table_name='Live'
) then
   drop index Live.Live2_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Live_FK'
     and t.table_name='Live'
) then
   drop index Live.Live_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Discuss_FK'
     and t.table_name='Paper'
) then
   drop index Paper.Discuss_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Relationship_10_FK'
     and t.table_name='Paper'
) then
   drop index Paper.Relationship_10_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Relationship_14_FK'
     and t.table_name='Paper'
) then
   drop index Paper.Relationship_14_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Relationship_18_FK'
     and t.table_name='Paper'
) then
   drop index Paper.Relationship_18_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Relationship_20_FK'
     and t.table_name='Relation'
) then
   drop index Relation.Relationship_20_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Relationship_13_FK'
     and t.table_name='Theme'
) then
   drop index Theme.Relationship_13_FK
end if;

if exists(
   select 1 from sys.sysindex i, sys.systable t
   where i.table_id=t.table_id 
     and i.index_name='Relationship_15_FK'
     and t.table_name='Theme'
) then
   drop index Theme.Relationship_15_FK
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Adress'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Adress
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Author'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Author
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Book'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Book
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Conference'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Conference
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Create_book'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Create_book
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Create_paper'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Create_paper
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Genre/Style'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table "Genre/Style"
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Graph'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Graph
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Language'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Language
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Live'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Live
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Man'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Man
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Paper'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Paper
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Publishing_house'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Publishing_house
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Relation'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Relation
end if;

if exists(
   select 1 from sys.systable 
   where table_name='Theme'
     and table_type in ('BASE', 'GBL TEMP')
) then
    drop table Theme
end if;

/*==============================================================*/
/* Table: Adress                                                */
/*==============================================================*/
create table Adress 
(
    id_adress            integer                        not null,
    Country              varchar,
    city                 varchar,
    street               varchar,
    house                varchar,
    housing              integer,
    flat                 tinyint,
    "index"              integer,
    "e-mail"             varchar,
    icq                  integer,
    constraint PK_ADRESS primary key (id_adress)
);

/*==============================================================*/
/* Table: Author                                                */
/*==============================================================*/
create table Author 
(
    ID_author            integer                        not null,
    ID_publ_house        integer,
    ID_conf              integer,
    Literature           varchar,
    "Co-authors"         varchar,
    constraint PK_AUTHOR primary key (ID_author)
);

/*==============================================================*/
/* Index: Participate_FK                                        */
/*==============================================================*/
create  index Participate_FK on Author (
ID_conf ASC
);

/*==============================================================*/
/* Index: Relationship_17_FK                                    */
/*==============================================================*/
create  index Relationship_17_FK on Author (
ID_publ_house ASC
);

/*==============================================================*/
/* Table: Book                                                  */
/*==============================================================*/
create table Book 
(
    ID_book              integer                        not null,
    ID_theme             integer,
    ID_conf              integer,
    ID_graph             integer,
    Name_book            varchar                        not null,
    Genre_book           varchar,
    Year_of_publ         smallint                       not null,
    Publ_house           varchar                        not null,
    Numb_page            tinyint                        not null,
    Short_desc           long varchar                   not null,
    Lang_book            varchar,
    constraint PK_BOOK primary key (ID_book)
);

/*==============================================================*/
/* Index: Relationship_12_FK                                    */
/*==============================================================*/
create  index Relationship_12_FK on Book (
ID_theme ASC
);

/*==============================================================*/
/* Index: Relationship_16_FK                                    */
/*==============================================================*/
create  index Relationship_16_FK on Book (
ID_conf ASC
);

/*==============================================================*/
/* Index: Relationship_19_FK                                    */
/*==============================================================*/
create  index Relationship_19_FK on Book (
ID_graph ASC
);

/*==============================================================*/
/* Table: Conference                                            */
/*==============================================================*/
create table Conference 
(
    ID_conf              integer                        not null,
    Name_conf            varchar                        not null,
    Time_conf            varchar                        not null,
    Targets              varchar                        not null,
    Orgkom               long varchar                   not null,
    Subjects             long varchar                   not null,
    Important_dates      varchar                        not null,
    Place_conf           varchar                        not null,
    "References"         varchar                        not null,
    Contacts             varchar                        not null,
    Materials_conf       long varchar                   not null,
    constraint PK_CONFERENCE primary key (ID_conf)
);

/*==============================================================*/
/* Table: Create_book                                           */
/*==============================================================*/
create table Create_book 
(
    ID_author            integer                        not null,
    ID_book              integer                        not null,
    constraint PK_CREATE_BOOK primary key (ID_author, ID_book)
);

/*==============================================================*/
/* Index: Create_book_FK                                        */
/*==============================================================*/
create  index Create_book_FK on Create_book (
ID_author ASC
);

/*==============================================================*/
/* Index: Create_book2_FK                                       */
/*==============================================================*/
create  index Create_book2_FK on Create_book (
ID_book ASC
);

/*==============================================================*/
/* Table: Create_paper                                          */
/*==============================================================*/
create table Create_paper 
(
    ID_author            integer                        not null,
    ID_paper             integer                        not null,
    constraint PK_CREATE_PAPER primary key (ID_author, ID_paper)
);

/*==============================================================*/
/* Index: Create_paper_FK                                       */
/*==============================================================*/
create  index Create_paper_FK on Create_paper (
ID_author ASC
);

/*==============================================================*/
/* Index: Create_paper2_FK                                      */
/*==============================================================*/
create  index Create_paper2_FK on Create_paper (
ID_paper ASC
);

/*==============================================================*/
/* Table: "Genre/Style"                                         */
/*==============================================================*/
create table "Genre/Style" 
(
    "ID_Genre/Style"     integer                        not null,
    ID_paper             integer,
    ID_book              integer,
    "Genre/Style_name"   varchar                        not null,
    "Genre/Style_desc"   long varchar,
    constraint "PK_GENRE/STYLE" primary key ("ID_Genre/Style")
);

/*==============================================================*/
/* Index: Relationship_11_FK                                    */
/*==============================================================*/
create  index Relationship_11_FK on "Genre/Style" (
ID_paper ASC
);

/*==============================================================*/
/* Index: Book_belong_FK                                        */
/*==============================================================*/
create  index Book_belong_FK on "Genre/Style" (
ID_book ASC
);

/*==============================================================*/
/* Table: Graph                                                 */
/*==============================================================*/
create table Graph 
(
    ID_graph             integer                        not null,
    N_sent               integer,
    From_conc            varchar,
    To_conc              varchar,
    constraint PK_GRAPH primary key (ID_graph)
);

/*==============================================================*/
/* Table: Language                                              */
/*==============================================================*/
create table Language 
(
    ID_lang              integer                        not null,
    ID_author            integer,
    ID_book              integer,
    ID_paper             integer,
    Name_lang            varchar,
    constraint PK_LANGUAGE primary key (ID_lang)
);

/*==============================================================*/
/* Index: Speak_FK                                              */
/*==============================================================*/
create  index Speak_FK on Language (
ID_author ASC
);

/*==============================================================*/
/* Index: Translate_paper_FK                                    */
/*==============================================================*/
create  index Translate_paper_FK on Language (
ID_paper ASC
);

/*==============================================================*/
/* Index: Translate_book_FK                                     */
/*==============================================================*/
create  index Translate_book_FK on Language (
ID_book ASC
);

/*==============================================================*/
/* Table: Live                                                  */
/*==============================================================*/
create table Live 
(
    ID_man               integer                        not null,
    id_adress            integer                        not null,
    constraint PK_LIVE primary key (ID_man, id_adress)
);

/*==============================================================*/
/* Index: Live_FK                                               */
/*==============================================================*/
create  index Live_FK on Live (
ID_man ASC
);

/*==============================================================*/
/* Index: Live2_FK                                              */
/*==============================================================*/
create  index Live2_FK on Live (
id_adress ASC
);

/*==============================================================*/
/* Table: Man                                                   */
/*==============================================================*/
create table Man 
(
    ID_man               integer                        not null,
    Name                 varchar                        not null,
    Patronymic           varchar                        not null,
    Surname              varchar                        not null,
    constraint PK_MAN primary key (ID_man)
);

/*==============================================================*/
/* Table: Paper                                                 */
/*==============================================================*/
create table Paper 
(
    ID_paper             integer                        not null,
    "ID_Genre/Style"     integer,
    ID_theme             integer,
    ID_conf              integer,
    ID_graph             integer,
    Name_paper           varchar                        not null,
    Style_paper          varchar,
    Publ_house           varchar,
    Publ_source          varchar                        not null,
    Number_page          tinyint,
    Short_desc           long varchar                   not null,
    Lang_paper           varchar                        not null,
    Publ_time            varchar                        not null,
    constraint PK_PAPER primary key (ID_paper)
);

/*==============================================================*/
/* Index: Relationship_10_FK                                    */
/*==============================================================*/
create  index Relationship_10_FK on Paper (
"ID_Genre/Style" ASC
);

/*==============================================================*/
/* Index: Relationship_14_FK                                    */
/*==============================================================*/
create  index Relationship_14_FK on Paper (
ID_theme ASC
);

/*==============================================================*/
/* Index: Discuss_FK                                            */
/*==============================================================*/
create  index Discuss_FK on Paper (
ID_conf ASC
);

/*==============================================================*/
/* Index: Relationship_18_FK                                    */
/*==============================================================*/
create  index Relationship_18_FK on Paper (
ID_graph ASC
);

/*==============================================================*/
/* Table: Publishing_house                                      */
/*==============================================================*/
create table Publishing_house 
(
    ID_publ_house        integer                        not null,
    Name_publ_house      varchar                        not null,
    Authors              varchar                        not null,
    Publ_house_desc      long varchar,
    constraint PK_PUBLISHING_HOUSE primary key (ID_publ_house)
);

/*==============================================================*/
/* Table: Relation                                              */
/*==============================================================*/
create table Relation 
(
    ID_rel               integer                        not null,
    ID_graph             integer,
    Rel_name             varchar,
    Rel_desc             varchar,
    constraint PK_RELATION primary key (ID_rel)
);

/*==============================================================*/
/* Index: Relationship_20_FK                                    */
/*==============================================================*/
create  index Relationship_20_FK on Relation (
ID_graph ASC
);

/*==============================================================*/
/* Table: Theme                                                 */
/*==============================================================*/
create table Theme 
(
    ID_theme             integer                        not null,
    ID_book              integer,
    ID_paper             integer,
    Theme_name           varchar                        not null,
    ID_parent            varchar                        not null,
    Theme_desc           long varchar                   not null,
    constraint PK_THEME primary key (ID_theme)
);

/*==============================================================*/
/* Index: Relationship_13_FK                                    */
/*==============================================================*/
create  index Relationship_13_FK on Theme (
ID_book ASC
);

/*==============================================================*/
/* Index: Relationship_15_FK                                    */
/*==============================================================*/
create  index Relationship_15_FK on Theme (
ID_paper ASC
);

